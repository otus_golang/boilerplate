package main

import (
	"gitlab.com/otus_golang/boilerplate/cmd"
)

func main() {
	cmd.Execute()
}
