package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/otus_golang/boilerplate/config"
	"gitlab.com/otus_golang/boilerplate/logger"
	"go.uber.org/zap"
	"log"
)

var c *config.Config
var l *zap.Logger

func init() {
	initConfig()
	initLogger(c)
}

func initConfig() {
	var err error

	c, err = config.GetConfig()

	if err != nil {
		log.Fatalf("unable to load config: %v", err)
	}
}

func initLogger(c *config.Config) {
	var err error
	l, err = logger.GetLogger(c)

	if err != nil {
		log.Fatalf("unable to load logger: %v", err)
	}
}

var rootCmd = &cobra.Command{
	Use:   "boilerplate",
	Short: "boilerplate service",
	Long:  `boilerplate service`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print("Hello, world!")
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("root execute error: %v", err)
	}
}
