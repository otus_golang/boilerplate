module gitlab.com/otus_golang/boilerplate

go 1.13

require (
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	go.uber.org/zap v1.10.0
)
